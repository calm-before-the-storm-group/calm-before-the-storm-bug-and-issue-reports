Please use this page to submit issues for the Calm Before the Storm mod for Hearts of Iron IV.

How to submit Issues:

    1. On this page, click "Issues" in toolbar on the left of the screen.

    2. Click "New Issue"

    3. Give it a Title

        a. Use the following format for titles

        b. TAG: Brief description of the problem

        c. You can use GENERAL if it is a general problem

        d. Make your title detailed. Something like "GER: focus problem" tells us very little.
        
    4. Give us a detailed description of the problem. Please be as detailed as possible. You can copy-and-paste screenshots into the description box, and we recommend you do so.

        a. Make sure you include the game version, mod version, active DLC, and any other active mods!

    5. Assign a developer. Please use our discord to check who to assign.

    6. Assign a Label (see "Issue Labels")

    7. Click "Submit Issue"

You can comment on other issues or react to them.

Please make sure YOUR ISSUE IS NOT A DUPLICATE BEFORE REPORTING. If you find that you have a duplicate issue, you can react to the issue or comment on it to let us know you're having the same problem.



Issue Labels:

    AI: There is a problem with how the AI plays the mod. It is possible that these issues are base game problems, but report them anyways if you think it isn't.

    Balance: There is a problem with the balance. A country might be over or underpowered in general, or an effect might be too strong or weak, etc.

    GFX: Graphics are missing or are treated poorly by the interface (wrong position or some other kinds of janky behaviour).
    
    Logic: The code works, but the outcome is not as intended. For example, 2+2 = 6 would be a logic error, even if the code is technically correct. In game, this would mean, for example, that a focus check doesn't work, or the wrong events are triggered.

    Miscellaneous: Issues that do not match other labels. Please try to use the other labels first.

    Missing Localization: Text is missing.

    Suggestions: Suggestions for fixes, changes, or additions. Try to make your suggestions as detailed as possible.

    Syntax: The code is actually broken. This is usually not visible on the front end and the game will usually throw an error for them, but if you find these errors, please report them.
    
    Typo: There is a typo in the text.
